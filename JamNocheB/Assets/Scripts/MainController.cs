﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainController : MonoBehaviour
{

    public void PlayWithVR()
    {
        PlayerPrefs.SetInt("VR", 1);
        PlayerPrefs.SetInt("Meteorito",20);
        SceneManager.LoadScene(1);
    }

    public void PlayWithoutVR()
    {
        PlayerPrefs.SetInt("VR", 0);
        PlayerPrefs.SetInt("Meteorito", 40);
        SceneManager.LoadScene(1);
    }
}
