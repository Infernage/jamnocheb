﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour
{

    private static AudioClip asfixia, click, golpe, motor, recarga, respiracion;
    


    void Awake()
    {
        asfixia = (AudioClip)Resources.Load("Audio/Asfixia");
        click = (AudioClip)Resources.Load("Audio/Click");
        golpe = (AudioClip)Resources.Load("Audio/Golpe");
        motor = (AudioClip)Resources.Load("Audio/Motor");
        recarga = (AudioClip)Resources.Load("Audio/Recarga de energia");
        respiracion = (AudioClip)Resources.Load("Audio/Respiracion");

    }

    public static AudioClip PlayAsfixia()
    {
        return asfixia;
    }

    public static AudioClip PlayClick()
    {
        return click;
    }

    public static AudioClip PlayGolpe()
    {
        return golpe;
    }

    public static AudioClip PlayMotor()
    {
        return motor;
    }

    public static AudioClip PlayRecarga()
    {
        return recarga;
    }

    public static AudioClip PlayRespiracion()
    {
        return respiracion;
    }


}
