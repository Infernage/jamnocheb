﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Generator : MonoBehaviour
{
    public float distanceToGenerate = 50, maxDistance = 50, minDistance, numberObjects = 20;
    public float distance = 0;
    private Vector3 lastPosition;
    private Vector3 vDistance;
    private GameObject meteorito, energia;
    private bool generated = false;
    private float timer = 10;

    public void Awake()
    {
        meteorito = (GameObject)Resources.Load("Prefabs/Meteorito");
        energia = (GameObject)Resources.Load("Prefabs/Energia");
    }

    public void Start()
    {
        // Generate();
        //GenerateSphere();

    }

    public void Update()
    {
        timer += Time.deltaTime;
        if (timer > 10)
        {
            GenerateSphere();
            //generated = true;
            timer = 0;
        }
    }

    private void GenerateSphere()
    {
        for (int i = 0; i < 20; i++)
        {
            Vector3 num = Random.insideUnitSphere * 1000;

            GameObject obj = Instantiate(meteorito, Vector3.zero + num, Random.rotation) as GameObject;
            float r = Random.Range(0, 20);
            Vector3 random = new Vector3(r, r, r);
            obj.transform.localScale = random;
            Rigidbody rb = obj.GetComponent<Rigidbody>();
            rb.angularVelocity = Random.insideUnitSphere * 5;
            Vector3 torque = new Vector3(Random.Range(-200, 200), Random.Range(-200, 200), Random.Range(-200, 200));
            rb.AddTorque(torque);
            rb.AddForce(torque * rb.mass, ForceMode.Impulse);
        }

    }

}
