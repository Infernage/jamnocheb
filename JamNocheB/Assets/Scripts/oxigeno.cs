﻿using UnityEngine;
using System.Collections;

public class oxigeno : MonoBehaviour
{

    public float Oxigeno = 100;
    private float timer = 0;
    private GameObject pilot;
    private float pilotTimer = 0;
    public float pilotInitialTimer = 30;
    public float pilotTotalTimer = 90;
    public AudioSource pitido;
    //private GameController gameController;


    void Awake()
    {
        pilot = GameObject.Find("Canvas/Pilot");
        pilot.SetActive(false);
    }


    // Use this for initialization
    void Start()
    {
        Invoke("EnablePilot", pilotInitialTimer);
    }

    public void EnablePilot()
    {
        pilot.SetActive(true);
        float delta = (pilotTimer - pilotInitialTimer) / pilotTotalTimer;
        pitido.Play();
        if (Oxigeno > 0) Invoke("DisablePilot", 1 - delta);
    }

    public void DisablePilot()
    {
        pilot.SetActive(false);
        float delta = (pilotTimer - pilotInitialTimer) / pilotTotalTimer;
        if (Oxigeno > 0) Invoke("EnablePilot", 1 - delta);
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= 1)
        {
            Oxigeno -= 0.83f;
            timer = 0;
        }
        if (Oxigeno <= 0 && CharacterController.state == CharacterController.UIState.Playing)
        {
            CharacterController.state = CharacterController.UIState.GameOverVictory;
            FindObjectOfType<CharacterController>().gameOverUI.SetActive(true);
            Time.timeScale = 0;
        }
        pilotTimer += Time.deltaTime;
    }
}
