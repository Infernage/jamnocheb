﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{
    private Cardboard cardBoard;

    void Awake()
    {
        cardBoard = GameObject.Find("CardboardMain").GetComponent<Cardboard>();
        if (PlayerPrefs.GetInt("VR") == 0)
        {
            cardBoard.VRModeEnabled = false;
        }
        else {
            cardBoard.VRModeEnabled = true;
        }

    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
