﻿using UnityEngine;
using System.Collections;

public class StarController : MonoBehaviour
{
    private Light light;
    public float max;
    float random;
    private GameObject player;

    // Use this for initialization
    void Start()
    {
        light = this.GetComponent<Light>();
        player = GameObject.Find("CardboardMain");
        random = Random.Range(0.0f, 8f);
    }

    // Update is called once per frame
    void Update()
    {
        this.gameObject.transform.LookAt(player.transform);
        float noise = Mathf.PerlinNoise(random, Time.time);
        light.intensity = Mathf.Lerp(2.0f, max, noise);
    }
}
