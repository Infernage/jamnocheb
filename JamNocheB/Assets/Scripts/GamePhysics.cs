﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GamePhysics : MonoBehaviour
{
    public double mass = 1.98855e+32; // Default bh mass is ~2xe32 kg

    private const double G = 6.674e-11;

    public void Awake()
    {
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        List<Rigidbody> rbs = new List<Rigidbody>();
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("rigidBody"))
        {
            Rigidbody rb = obj.GetComponent<Rigidbody>();
            if (rb != null && rb != GetComponent<Rigidbody>() && !rbs.Contains(rb))
            {
                rbs.Add(rb);
                float offset = Vector3.Distance(transform.position, obj.transform.position);
                double F = G * (mass * rb.mass) / Mathf.Pow(offset, 2);
                Vector3 normalDir = (transform.position - obj.transform.position).normalized;
                Vector3d normalForce = new Vector3d(normalDir) * F;
                rb.AddForce(new Vector3((float) normalForce.x, (float) normalForce.y, (float) normalForce.z), ForceMode.Force);
            }
        }
        // Energy unused
        /*foreach (GameObject obj in GameObject.FindGameObjectsWithTag("energy"))
        {
            Rigidbody rb = obj.GetComponent<Rigidbody>();
            if (rb != null && rb != GetComponent<Rigidbody>() && !rbs.Contains(rb))
            {
                rbs.Add(rb);
                float offset = Vector3.Distance(transform.position, obj.transform.position);
                double F = G * (mass * rb.mass) / Mathf.Pow(offset, 2);
                Vector3 normalDir = (obj.transform.position - transform.position).normalized;
                Vector3d normalForce = new Vector3d(normalDir) * F;
                rb.AddForce(new Vector3((float)normalForce.x, (float)normalForce.y, (float)normalForce.z));
            }
        }*/
    }

    void finish()
    {
        CharacterController.state = CharacterController.UIState.GameOverVictory;
        FindObjectOfType<CharacterController>().gameOverUI.SetActive(true);
        FindObjectOfType<CharacterController>().panel.SetActive(true);
        Time.timeScale = 0;
    }

    public void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.name.Equals("Character"))
        {
            // Game over
            CameraFade.StartAlphaFade(Color.black, false, 1, 0, finish);
        }
        else if (collision.gameObject.tag.Equals("rigidBody"))
        {
            Destroy(collision.gameObject);
        }
    }
}
