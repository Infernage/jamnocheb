﻿using UnityEngine;
using System.Collections;

public class ArrowController : MonoBehaviour
{

    GameObject transbordador;

    void Awake()
    {
        transbordador = GameObject.FindGameObjectWithTag("Finish");
    }


    // Update is called once per frame
    void Update()
    {
        this.transform.LookAt(transbordador.transform);
    }
}
