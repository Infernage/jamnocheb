﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CharacterController : MonoBehaviour
{
    public double speed = 0;
    public int drain = 1;
    public int energyRestore = 10;
    public float maxCharLocationX, minCharLocationX;
    public float maxCharLocationY, minCharLocationY;
    public float maxCharLocationZ, minCharLocationZ;
    public float maxShipLocationX, minShipLocationX;
    public float maxShipLocationY, minShipLocationY;
    public float maxShipLocationZ, minShipLocationZ;
    private int life = 3;
    private float timer = 0;
    private Animator anim;
    private Slider energyBar;
    public AudioSource respirar, motor, golpe, recarga, axfixia, click;

    private GameObject victoryUI;
    public GameObject gameOverUI;
    private GameObject creditsUI;
    private GameObject newGameUI;
    public GameObject panel;

    public enum UIState
    {
        Menu,
        Playing,
        GameOverVictory,
        Credits
    }
    public static UIState state;

    // Map: 197040 cm radius (394080 cm diameter)
    // BL: 39020 cm radius
    // Playable space: 355060 cm length
    // Player ubication spawn: 50000 length cm x 100000 diameter cm
    // Ship ubication spawn: 30000 length cm x 50000 diameter cm

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("rigidBody") && state == UIState.Playing)
        {
            life--;
            golpe.Play();
            switch (life)
            {
                case 2:
                    anim.Play("Cristal", 1);
                    break;
                case 1:
                    anim.Play("Cristal2", 1);
                    break;
                case 0:
                    anim.Play("Cristal3", 1);
                    axfixia.Play();
                    state = UIState.GameOverVictory;
                    gameOverUI.SetActive(true);
                    Time.timeScale = 0;
                    break;
            }
        }
    }

    public void OnTriggerEnter(Collider collision)
    {
        if (state != UIState.Playing) return;
        if (collision.gameObject.tag.Equals("energy"))
        {
            energyBar.value += energyRestore;
            recarga.Play();
            Destroy(collision.gameObject);
        }
        else if (collision.gameObject.tag.Equals("Finish"))
        {
            Time.timeScale = 0;
            victoryUI.SetActive(true);
            state = UIState.GameOverVictory;
        }
    }

    public void Awake()
    {
        if (speed <= 0) speed = FindObjectOfType<GamePhysics>().mass * 2;
        anim = GameObject.Find("Glass").GetComponent<Animator>();
        energyBar = GameObject.Find("Canvas/EnergyBar/Fill").GetComponent<Slider>();
        victoryUI = GameObject.Find("Canvas/Victory");
        victoryUI.SetActive(false);
        gameOverUI = GameObject.Find("Canvas/GameOver");
        gameOverUI.SetActive(false);
        creditsUI = GameObject.Find("Canvas/Credits");
        creditsUI.SetActive(false);
        panel = GameObject.Find("Canvas/Panel");
        panel.SetActive(false);
        newGameUI = GameObject.Find("Canvas/NewGame");
        state = UIState.Menu;
    }

    // Use this for initialization
    void Start()
    {
        float cx = Random.Range(minCharLocationX, maxCharLocationX);
        float cy = Random.Range(minCharLocationY, maxCharLocationY);
        float cz = Random.Range(minCharLocationZ, maxCharLocationZ);
        float sx = Random.Range(minShipLocationX, maxShipLocationX);
        float sy = Random.Range(minShipLocationY, maxShipLocationY);
        float sz = Random.Range(minShipLocationZ, maxShipLocationZ);

        transform.position = new Vector3(cx, cy, cz);
        GameObject.FindGameObjectWithTag("Finish").transform.position = new Vector3(sx, sy, sz);

        Time.timeScale = 0;
    }

    private void GamePlay()
    {
        if (energyBar.value > 0)
        {
            // Apply force to character where is looking to
            GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * (float) speed * Time.deltaTime, ForceMode.Impulse);
            energyBar.value -= drain;
        }
        if (energyBar.value == 0)
        {
            energyBar.enabled = false;
            state = UIState.GameOverVictory;
            gameOverUI.SetActive(true);
            Time.timeScale = 0;
        }
        timer += Time.deltaTime;
        if (timer >= 4)
        {
            anim.Play("Vaho", 0);
            respirar.Play();
            timer = 0;
        }
    }

    private void GameMenu()
    {
        if (Input.GetMouseButtonDown(0))
        {
            newGameUI.SetActive(false);
            Time.timeScale = 1;
            state = UIState.Playing;
            click.Play();
        }
    }

    private void GameCredits()
    {
        if (Input.GetMouseButtonDown(0))
        {
            click.Play();
            SceneManager.LoadScene(1);
        }
    }

    private void GameOverVictory()
    {
        if (respirar.isPlaying) respirar.Stop();
        if (Input.GetMouseButtonDown(0))
        {
            gameOverUI.SetActive(false);
            victoryUI.SetActive(false);
            creditsUI.SetActive(true);
            state = UIState.Credits;
            click.Play();
        }
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case UIState.Menu:
                GameMenu();
                break;
            case UIState.Credits:
                GameCredits();
                break;
            case UIState.GameOverVictory:
                GameOverVictory();
                break;
            case UIState.Playing:
                GamePlay();
                break;
        }
    }

}
